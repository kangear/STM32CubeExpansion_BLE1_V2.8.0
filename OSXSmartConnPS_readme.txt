-----------
readme.txt
-----------
The OSXSmartConnPS V1.6.0 package is an add-on to the X-CUBE-BLE1 V2.8.0 software
expansion for STM32Cube.

This package contains the BlueNRG library for BLE Profiles and four sample
applications.

For instructions on how to use this package, see the 'How to Install' section
below.
For instructions on how to use the sample applications, see the files:
- Projects\Multi\Applications\Profiles_LowPower\readme.txt
- Projects\Multi\Applications\Profiles_DMA_LowPower\readme.txt
- Projects\Multi\Applications\Profiles_Central\readme.txt
- Projects\Multi\Applications\BLE_ANCS\readme.txt

---------
Contents
---------
This package contains:
 * OSXSmartConnPS_readme.txt ............ This file
 
 * OSXSmartConnPS_Release_Notes.html .... Release Notes about the OSXSmartConnPS package
 
 * _htmresc ............................. html resources used by the Release Notes file
   
 * Documentation
   * OSXSmartConnPS.chm ................. Documentation of the source code included in 
                                          this package

 * Middlewares\ST\STM32_BlueNRG
   * Prof_Periph_Lib .................... BlueNRG library for BLE Peripheral Profiles
   * Prof_Periph ........................ BlueNRG Peripheral include files and
                                          timer.c file implementing Timer tasks for 
                                          timeout functionality
   * Prof_Centr_Lib ..................... BlueNRG library for BLE Central Profiles
   * Prof_Centr ......................... BlueNRG Central include files and
                                          timer.c file implementing Timer tasks for 
                                          timeout functionality
   * BLE_ANCS_Lib ....................... BlueNRG library for BLE Apple Notification 
                                          Center Service (ANCS) profile
                                          
 * Projects\Multi\Applications\Profiles_LowPower
   * Profiles_LowPower .................. Sample application showing how to use BLE
                                          Peripheral Profiles (see the included readme.txt
                                          for description). This sample application uses
                                          the STM32 Cube High Level APIs to set the
                                          low power modes.
                                          This is the recommended application if your
                                          goals are evaluating the BLE Peripheral Profiles
                                          in a simple way or easily porting the code to
                                          different STM32 families.

 * Projects\Multi\Applications\Profiles_DMA_LowPower
   * Profiles_DMA_LowPower .............. Sample application showing how to use BLE
                                          Peripheral Profiles (see the included readme.txt
                                          for description). This sample application uses
                                          the STM32 Cube Low Level low power 
                                          optimizations along with the DMA module.
                                          This application allows the most efficient
                                          power consumption and is heavily optimized.
                                          It is less easy to understand compared
                                          to the Profiles_LowPower application.
                               
 * Projects\Multi\Applications\Profiles_Central
   * Profiles_Central ................... Sample application showing how to use BLE
                                          Central Profiles (see the included readme.txt
                                          for description).
                                          This application allows evaluating the BLE Central
                                          profiles in a simple way and easily porting the code
                                          to different STM32 families.
                                          
 * Projects\Multi\Applications\BLE_ANCS
   * BLE_ANCS ........................... Sample application showing how to use BLE Apple Notification
                                          Center Service (ANCS) Profile (see the included readme.txt for
                                          description). This application allows evaluating the BLE ANCS
                                          Profile in a simple way and easily porting the code to different
                                          STM32 families. 
                           
 * Utilities\Android_Software\Profiles_Central
   * STM32_BLE_Profiles.apk ............. App for Android to be used with both 
                                          Profiles_LowPower and Profiles_DMA_LowPower
                                          sample applications
   * license.txt ........................ License terms and conditions
   * readme.txt ......................... STM32_BLE_Profiles App description and usage
                                          instructions
                               
 * Utilities\PC_Software\ProfileCentralTool
   * profileCentral.jar ................ Java tool for Win/Lin/OSX PC to be used with 
                                         Profiles_Central sample application
   * readme.txt ........................ profileCentral.jar Java tool description and usage
                                         instructions                               
   
---------------
How to Install
---------------
* OPTION A
  1. Download and install the X-CUBE-BLE1 from http://www.st.com

  2. Select all the folders contained in this package and 'drag
     and drop' them in your X-CUBE-BLE1 folder. Then click "yes" to confirm
     that you want to merge the selected folders with the ones in your
     X-CUBE-BLE1.

  3. Enjoy!

* OPTION B
  1. Download and install the X-CUBE-BLE1 from http://www.st.com
  
  2. copy $THIS_PACKAGE\OSXSmartConnPS_Release_Notes.html
     to   $X-CUBE-BLE1_INST_PATH\.

  3. copy $THIS_PACKAGE\Documentation\OSXSmartConnPS.chm 
     to   $X-CUBE-BLE1_INST_PATH\Documentation\.

  4. copy $THIS_PACKAGE\Middlewares\ST\STM32_BlueNRG\Prof_Centr_Lib
     to   $X-CUBE-BLE1_INST_PATH\Middlewares\ST\STM32_BlueNRG\.

  5. copy $THIS_PACKAGE\Middlewares\ST\STM32_BlueNRG\Prof_Centr 
     to   $X-CUBE-BLE1_INST_PATH\Middlewares\ST\STM32_BlueNRG\.
     
  6. copy $THIS_PACKAGE\Middlewares\ST\STM32_BlueNRG\Prof_Periph_Lib
     to   $X-CUBE-BLE1_INST_PATH\Middlewares\ST\STM32_BlueNRG\.

  7. copy $THIS_PACKAGE\Middlewares\ST\STM32_BlueNRG\Prof_Periph
     to   $X-CUBE-BLE1_INST_PATH\Middlewares\ST\STM32_BlueNRG\.
     
  8. copy $THIS_PACKAGE\Middlewares\ST\STM32_BlueNRG\BLE_ANCS_Lib
     to   $X-CUBE-BLE1_INST_PATH\Middlewares\ST\STM32_BlueNRG\.
     
  9. copy $THIS_PACKAGE\Projects\Multi\Applications\Profiles_Central 
     to   $X-CUBE-BLE1_INST_PATH\Projects\Multi\Applications\.

 10. copy $THIS_PACKAGE\Projects\Multi\Applications\Profiles_LowPower 
     to   $X-CUBE-BLE1_INST_PATH\Projects\Multi\Applications\.

 11. copy $THIS_PACKAGE\Projects\Multi\Applications\Profiles_DMA_LowPower 
     to   $X-CUBE-BLE1_INST_PATH\Projects\Multi\Applications\.
     
 12. copy $THIS_PACKAGE\Projects\Multi\Applications\BLE_ANCS 
     to   $X-CUBE-BLE1_INST_PATH\Projects\Multi\Applications\.    

 13. copy $THIS_PACKAGE\Utilities
     to   $X-CUBE-BLE1_INST_PATH\.

 14. Enjoy!

_______________________________________________________________________________
- (C) COPYRIGHT 2016 STMicroelectronics                   ****END OF FILE**** -
